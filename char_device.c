#include <linux/atomic.h>
#include <linux/cdev.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <asm/errno.h>

#define SUCCESS 0
#define DEVICE_NAME "chardev"
#define BUF_LEN 80

/* inode: estructura de datos fundamental que contiene información sobre un archivo o directorio */
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char __user *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char __user *, size_t, loff_t *);
static int major;
enum {
  CDEV_NOT_USED = 0,
  CDEV_EXCLUSIVE_OPEN = 1,
};
static atomic_t already_open = ATOMIC_INIT(CDEV_NOT_USED); /* Used to prevent multiple access to device */
static char msg[BUF_LEN];
static unsigned long buffer_size = 0;
static struct class *cls;
static struct file_operations chardev_fops = { /* containing pointers to functions that are used to implement read, write, open*/
  .read = device_read,
  .write = device_write,
  .open = device_open,
  .release = device_release,
};

int init_module(void)
{
  major = register_chrdev(0, DEVICE_NAME, &chardev_fops);
  if (major < 0) {
    pr_alert("Registering char device failed with %d\n", major);
    return major;
  }
  pr_info("I was assigned major number %d.\n", major);
  cls = class_create(THIS_MODULE, DEVICE_NAME);
  device_create(cls, NULL, MKDEV(major, 0), NULL, DEVICE_NAME);
  pr_info("Device created on /dev/%s\n", DEVICE_NAME);
  return SUCCESS;
}

void cleanup_module(void)
{
  device_destroy(cls, MKDEV(major, 0));
  class_destroy(cls);
  unregister_chrdev(major, DEVICE_NAME);
  pr_info("Device unregistered: %s\n", DEVICE_NAME);
}

static int device_open(struct inode *inode, struct file *file)
{
  if (atomic_cmpxchg(&already_open, CDEV_NOT_USED, CDEV_EXCLUSIVE_OPEN))
    return -EBUSY;
  try_module_get(THIS_MODULE);
  return SUCCESS;
}

static int device_release(struct inode *inode, struct file *file)
{
  atomic_set(&already_open, CDEV_NOT_USED);
  module_put(THIS_MODULE);
  return SUCCESS;
}

static ssize_t device_read(struct file *filp, char __user *buffer, size_t length, loff_t *offset)
{
  if (*offset >= buffer_size)
    return 0;
  if (copy_to_user(buffer, msg, buffer_size))
    return -EFAULT;

  *offset += buffer_size;
  pr_info("Read: %s\n", buffer);
  return buffer_size;
}

static ssize_t device_write(struct file *filp, const char *buff, size_t len, loff_t *off)
{
  buffer_size = len > BUF_LEN ? BUF_LEN : len;
  memset(msg, 0, sizeof buffer_size);
  if (copy_from_user(msg, buff, buffer_size))
    return -EFAULT;
  msg[buffer_size] = '\0';
  *off += buffer_size;
  pr_info("Written: %s\n", msg);
  return buffer_size;
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("UNGS");
MODULE_DESCRIPTION("Char device que cuando le escribimos imprima en el kernel");

